//declaration of variables
let textToTry
let res
let firstLetter
let $body = document.querySelector('body');

//suppress class of the body
function suppressClass(element) {
    element.classList.remove('camel');
    element.classList.remove('pascal');
    element.classList.remove('kebab');
    element.classList.remove('snake');
}
//show result
function showRes(str){
    document.getElementById('textToTry').value = str;
}

//clean the text before any case transformation (put space instead of - or _ or before Uppercase letters) and save it in texToTry variable and suppress accents.
function cleanTxt() {
    textToTry = document.getElementById('textToTry').value
    textToTry = textToTry.replace(/[A-Z]/g, function (match) {
        return match = " " + match;
    });
    textToTry = textToTry.toLowerCase().replace(/([éèê])/g, "e").replace(/([àâ])/g, "a").replace(/([ùû])/g, "u").replace(/([ïî])/g, "i").replace(/([^a-zA-Z ])/g, " ");
}

//camel function
function camelize (){
    textToTry = textToTry.replace(/\W+(.)/g, function (match, chr) {
        return chr.toUpperCase();
    });
    firstLetter = textToTry[0];
    firstLetter = firstLetter.toLowerCase();
    res = firstLetter + textToTry.slice(1, textToTry.lenght);
}

//to camel case
document.getElementById('camelIt').addEventListener('click', function () {
    if (document.getElementById('textToTry').value) {
        cleanTxt();
        //camel case transformation
        camelize ();
        // show in the input the result
        showRes(res);
        //change body apearance with classes
        suppressClass($body);
        $body.classList.add('camel');
        document.getElementById('footerWave').setAttribute('d', "M0,192L11.4,181.3C22.9,171,46,149,69,165.3C91.4,181,114,235,137,266.7C160,299,183,309,206,272C228.6,235,251,149,274,106.7C297.1,64,320,64,343,69.3C365.7,75,389,85,411,85.3C434.3,85,457,75,480,101.3C502.9,128,526,192,549,202.7C571.4,213,594,171,617,149.3C640,128,663,128,686,122.7C708.6,117,731,107,754,112C777.1,117,800,139,823,144C845.7,149,869,139,891,154.7C914.3,171,937,213,960,224C982.9,235,1006,213,1029,176C1051.4,139,1074,85,1097,53.3C1120,21,1143,11,1166,42.7C1188.6,75,1211,149,1234,181.3C1257.1,213,1280,203,1303,213.3C1325.7,224,1349,256,1371,256C1394.3,256,1417,224,1429,208L1440,192L1440,320L1428.6,320C1417.1,320,1394,320,1371,320C1348.6,320,1326,320,1303,320C1280,320,1257,320,1234,320C1211.4,320,1189,320,1166,320C1142.9,320,1120,320,1097,320C1074.3,320,1051,320,1029,320C1005.7,320,983,320,960,320C937.1,320,914,320,891,320C868.6,320,846,320,823,320C800,320,777,320,754,320C731.4,320,709,320,686,320C662.9,320,640,320,617,320C594.3,320,571,320,549,320C525.7,320,503,320,480,320C457.1,320,434,320,411,320C388.6,320,366,320,343,320C320,320,297,320,274,320C251.4,320,229,320,206,320C182.9,320,160,320,137,320C114.3,320,91,320,69,320C45.7,320,23,320,11,320L0,320Z");
    }
});

//to pascal case
document.getElementById('pascalIt').addEventListener('click', function () {
    if (document.getElementById('textToTry').value) {
        cleanTxt();
        //pascal transformation
        camelize ();
        firstLetter = firstLetter.toUpperCase();
        res = firstLetter + textToTry.slice(1, textToTry.lenght);
        // show in the input the result
        showRes(res);
        //change body apearance with classes
        suppressClass($body);
        $body.classList.add('pascal');
        document.getElementById('footerWave').setAttribute('d', "M0,192L11.4,181.3C22.9,171,46,149,69,149.3C91.4,149,114,171,137,186.7C160,203,183,213,206,208C228.6,203,251,181,274,154.7C297.1,128,320,96,343,101.3C365.7,107,389,149,411,186.7C434.3,224,457,256,480,261.3C502.9,267,526,245,549,224C571.4,203,594,181,617,149.3C640,117,663,75,686,48C708.6,21,731,11,754,21.3C777.1,32,800,64,823,74.7C845.7,85,869,75,891,74.7C914.3,75,937,85,960,112C982.9,139,1006,181,1029,176C1051.4,171,1074,117,1097,128C1120,139,1143,213,1166,213.3C1188.6,213,1211,139,1234,101.3C1257.1,64,1280,64,1303,90.7C1325.7,117,1349,171,1371,192C1394.3,213,1417,203,1429,197.3L1440,192L1440,320L1428.6,320C1417.1,320,1394,320,1371,320C1348.6,320,1326,320,1303,320C1280,320,1257,320,1234,320C1211.4,320,1189,320,1166,320C1142.9,320,1120,320,1097,320C1074.3,320,1051,320,1029,320C1005.7,320,983,320,960,320C937.1,320,914,320,891,320C868.6,320,846,320,823,320C800,320,777,320,754,320C731.4,320,709,320,686,320C662.9,320,640,320,617,320C594.3,320,571,320,549,320C525.7,320,503,320,480,320C457.1,320,434,320,411,320C388.6,320,366,320,343,320C320,320,297,320,274,320C251.4,320,229,320,206,320C182.9,320,160,320,137,320C114.3,320,91,320,69,320C45.7,320,23,320,11,320L0,320Z");
    }
});

//to kebab case
document.getElementById('kebabIt').addEventListener('click', function () {
    if (document.getElementById('textToTry').value) {
        cleanTxt();
        //kebab transformation
        textToTry = textToTry.replace(/([ ])/g, "-");
        //suppress any starting hyphen
        if (textToTry[0] == '-') {
            textToTry = textToTry.slice(1, textToTry.lenght);
        }
        // show in the input the result
        showRes(textToTry);
        //change body apearance with classes
        suppressClass($body);
        $body.classList.add('kebab');
        document.getElementById('footerWave').setAttribute('d', "M0,32L11.4,37.3C22.9,43,46,53,69,96C91.4,139,114,213,137,250.7C160,288,183,288,206,282.7C228.6,277,251,267,274,261.3C297.1,256,320,256,343,218.7C365.7,181,389,107,411,96C434.3,85,457,139,480,186.7C502.9,235,526,277,549,288C571.4,299,594,277,617,250.7C640,224,663,192,686,186.7C708.6,181,731,203,754,218.7C777.1,235,800,245,823,208C845.7,171,869,85,891,80C914.3,75,937,149,960,154.7C982.9,160,1006,96,1029,64C1051.4,32,1074,32,1097,58.7C1120,85,1143,139,1166,186.7C1188.6,235,1211,277,1234,250.7C1257.1,224,1280,128,1303,96C1325.7,64,1349,96,1371,122.7C1394.3,149,1417,171,1429,181.3L1440,192L1440,320L1428.6,320C1417.1,320,1394,320,1371,320C1348.6,320,1326,320,1303,320C1280,320,1257,320,1234,320C1211.4,320,1189,320,1166,320C1142.9,320,1120,320,1097,320C1074.3,320,1051,320,1029,320C1005.7,320,983,320,960,320C937.1,320,914,320,891,320C868.6,320,846,320,823,320C800,320,777,320,754,320C731.4,320,709,320,686,320C662.9,320,640,320,617,320C594.3,320,571,320,549,320C525.7,320,503,320,480,320C457.1,320,434,320,411,320C388.6,320,366,320,343,320C320,320,297,320,274,320C251.4,320,229,320,206,320C182.9,320,160,320,137,320C114.3,320,91,320,69,320C45.7,320,23,320,11,320L0,320Z");
    }
});

document.getElementById('snakeIt').addEventListener('click', function () {
    if (document.getElementById('textToTry').value) {
        cleanTxt();
        //snake
        textToTry = textToTry.replace(/([ ])/g, "_");
        //suppress any starting hyphen
        if (textToTry[0] == '_') {
            textToTry = textToTry.slice(1, textToTry.lenght);
        }
        // show in the input the result
        showRes(textToTry);
        //change body apearance with classes
        suppressClass($body);
        $body.classList.add('snake');
        document.getElementById('footerWave').setAttribute('d', "M0,128L11.4,160C22.9,192,46,256,69,272C91.4,288,114,256,137,245.3C160,235,183,245,206,234.7C228.6,224,251,192,274,192C297.1,192,320,224,343,224C365.7,224,389,192,411,197.3C434.3,203,457,245,480,245.3C502.9,245,526,203,549,197.3C571.4,192,594,224,617,250.7C640,277,663,299,686,277.3C708.6,256,731,192,754,170.7C777.1,149,800,171,823,170.7C845.7,171,869,149,891,160C914.3,171,937,213,960,234.7C982.9,256,1006,256,1029,256C1051.4,256,1074,256,1097,261.3C1120,267,1143,277,1166,282.7C1188.6,288,1211,288,1234,277.3C1257.1,267,1280,245,1303,224C1325.7,203,1349,181,1371,197.3C1394.3,213,1417,267,1429,293.3L1440,320L1440,320L1428.6,320C1417.1,320,1394,320,1371,320C1348.6,320,1326,320,1303,320C1280,320,1257,320,1234,320C1211.4,320,1189,320,1166,320C1142.9,320,1120,320,1097,320C1074.3,320,1051,320,1029,320C1005.7,320,983,320,960,320C937.1,320,914,320,891,320C868.6,320,846,320,823,320C800,320,777,320,754,320C731.4,320,709,320,686,320C662.9,320,640,320,617,320C594.3,320,571,320,549,320C525.7,320,503,320,480,320C457.1,320,434,320,411,320C388.6,320,366,320,343,320C320,320,297,320,274,320C251.4,320,229,320,206,320C182.9,320,160,320,137,320C114.3,320,91,320,69,320C45.7,320,23,320,11,320L0,320Z");
    }
});